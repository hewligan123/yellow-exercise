/* globals jQuery */

(function ($) {
    /**
     * @constant {string}
     * @description URL to fetch movie data from
     */
    const LIST_URL = 'data.json';

    /**
     * @constant {String}
     * @description Sort order of data when none is supplied
     */
    const DEFAULT_SORT_ORDER = 'rank';

    /**
     * Load table templats from HTML
     * @return {Object} An object containing the table and row templates as strings
     */
    const getTemplates = () => {
        return {
            table: $('#table-template').text(),
            row: $('#row-template').text()
        };
    };

    /**
     * Load the movie list from the server
     * @return {promise} A jQuery promise for the data.
     */
    const fetchMovieList = () => {
        return $.get(LIST_URL);
    };

    /**
     * Create the content (rows) of the table
     * @param  {string} template A template for a single table row
     * @param  {array}  data     The movie data to populate the table
     * @return {object}          A jQuery object of the table body
     */
    const makeTableContents = (template, data) => {
        const $content = $('<tbody>');

        data.forEach((movie) => {
            const $newRow = $(template);

            $newRow.find('.rank')
                .text(movie.rank);

            $newRow.find('.title')
                .text(movie.title);

            $newRow.find('.year')
                .text(movie.year);
            
            $newRow.find('.director')
                .text(movie.director);

            $newRow.appendTo($content);
        });

        return $content;
    };

    /**
     * Set the current table sort button disabled
     * @param  {Object} $table    A jQuery reference to the table
     * @param  {String} sortOrder Field the table is currently sorted by
     * @return {void}
     */
    const setCurrentSortButton = ($table, sortOrder) => {
        $table.find('.sort-button').prop('disabled', false);
        $table.find('.sort-button-' + sortOrder).prop('disabled', true);
    };

    /**
     * Update the table with the given content
     * @param  {Object} $table    A jQuery reference to the table that needs updating
     * @param  {Object} $contents [description]
     * @return {void}
     */
    const setTableContents = ($table, $contents) => {
        $table.find('tbody')
            .replaceWith($contents);
    };

    /**
     * Create the movie table and attach event handlers
     * @param  {string} template          Template for the table
     * @param  {function} contentFunction A function to create the table contents with the given sort order
     * @return {Object}                   A jQuery reference to the new table
     */
    const createMovieTable = (template, contentFunction) => {
        const $table = $(template);

        const $tableContents = contentFunction(DEFAULT_SORT_ORDER);

        $table.find('.sort-button')
            .on('click', (e) => {
                const sortOrder = $(e.currentTarget).data('sort-action');
                const $sortedContents = contentFunction(sortOrder);

                setTableContents($table, $sortedContents);
                setCurrentSortButton($table, sortOrder);
            });
        
        setTableContents($table, $tableContents);
        setCurrentSortButton($table, DEFAULT_SORT_ORDER);

        return $table;
    };

    /**
     * Sort the movie data
     * @param  {array} data       The movies to be sorted
     * @param  {string} sortField What to sort the movies by
     * @return {arrray}           A new array containing the sorted movies
     */
    const sortData = (data, sortField) => {
        const sortedArray = data.slice(0);

        sortedArray.sort((a, b) => {
            if (a[sortField].localeCompare && b[sortField].localeCompare) {
                return a[sortField].localeCompare(b[sortField]);
            }

            return a[sortField] - b[sortField];
        });

        return sortedArray;
    };

    /**
     * Create the movie table and attach it to the DOM.
     * @param  {array} data  List of movies to populate the table
     * @return {void}
     */
    const attachTable = (data) => {
        const templates = getTemplates();

        const sortTableContents = (sortField) => {
            const sortedData = sortData(data, sortField);

            return makeTableContents(templates.row, sortedData);
        };

        const $table = createMovieTable(templates.table, sortTableContents);

        $table.appendTo('#main-content');
    };

    $(document).ready(() => {
        fetchMovieList().then(attachTable);
    });

}(jQuery));
