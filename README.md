*This is only intended to work in recent versions of Chrome/Firefox to avoid the complexities of legacy browser support in the short time available.*

# Setup #

A recent version of node.js is required.

1. Clone the repository.
2. In the project folder, run `npm install` to install the dependencies
3. In the project folder, run `npm start` to run the webserver

The page will be available at http://localhost:3000

The exercise file is in exercise-files/interview.js. The solution is in exercise-files/solution.js

For someone to do the exercise, remove solution.js and make sure that index.html is loading interview.js.

The goal is to complete the 4 commented out sections of code.

An additional goal would be to add the functionality so that if a sort button is clicked a second time, the sort order is reversed.