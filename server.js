/* globals __dirname */

const express = require('express');
const path = require('path');

const app = express();

app.use(express.static(path.join(__dirname, 'exercise-files')));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/static/index.html'));
});

app.listen(3000);
